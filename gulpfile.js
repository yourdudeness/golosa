//Подключаем модули галпа
const gulp = require('gulp'),
      sass = require('gulp-sass'),
      concat = require('gulp-concat'),
      autoprefixer = require('gulp-autoprefixer'),
      cleanCSS = require('gulp-clean-css'),
      terser = require('gulp-terser'),
      del = require('del'),
      imagemin = require('gulp-imagemin'),
      htmlmin = require('gulp-htmlmin'),
      imageminJpegRecompress = require('imagemin-jpeg-recompress'),
      browserSync = require('browser-sync').create();

const stylesFiles = [
   './src/sass/**/*.scss'
];

const jsFiles = [
   './src/js/**/*.js',
   '!./src/js/scripts.js'
];

const imgFiles = [
  './src/img/**/*.png',
  './src/img/**/*.jpg',
  './src/img/**/*.gif',
  './src/img/**/*.svg'
];

//Таск на стили CSS
function styles() {

   return gulp.src(stylesFiles)
   .pipe(sass({
      includePaths: require('node-normalize-scss').includePaths
    }))

   .pipe(concat('styles.css'))
   .pipe(autoprefixer({
      browserslistrc: ['last 2 versions'],
      cascade: false
   }))
   .pipe(cleanCSS({
      level: 2
   }))
   .pipe(gulp.dest('./build/css'))
   .pipe(browserSync.stream());
}


function scripts() {
   return gulp.src(jsFiles)
   .pipe(concat('scripts.js'))
   .pipe(terser())
   .pipe(gulp.dest('./build/js'))
   .pipe(browserSync.stream());
}

function minimg() {
   return gulp.src(imgFiles)
   .pipe(
        imagemin([
          imagemin.jpegtran({ progressive: true }),
          imagemin.optipng({ optimizationLevel: 5 }),
          imageminJpegRecompress({
             loops: 3,
             min: 60,
             max: 70,
             quality:'high'
           }),
          imagemin.svgo(),
          ]),
      )
   .pipe(gulp.dest('./build/img'))
}

function minhtml() {
  return gulp.src('./src/*.html')
 //   .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./build'));
}

function fonts() {
  return gulp.src('./src/fonts/**/*')
    .pipe(gulp.dest('./build/fonts'));
}
function audio() {
  return gulp.src('./src/audio/**/*')
    .pipe(gulp.dest('./build/audio'));
}

function clean() {
   return del(['build/css/**/*.css'])
}

function watch() {
   browserSync.init({
      server: {
          baseDir: "./build/",
          index: "index.html"
      }
  });

  gulp.watch('./src/sass/**/*.scss', styles);
  gulp.watch('./src/sass/css/**/*.css').on('change', browserSync.reload);
  gulp.watch(['./src/js/**/*.js', '!./src/js/scripts.js'], scripts);
  gulp.watch('./src/*.html').on('change', gulp.series(minhtml, browserSync.reload));
}

exports.styles = styles;
exports.scripts = scripts;
exports.delcss = clean;
exports.images = minimg;
exports.html = minhtml;
exports.fonts = fonts;
exports.watch = watch;
exports.build = gulp.series(clean, gulp.parallel(fonts, audio, minhtml, styles, scripts, minimg));
exports.dev = gulp.series(exports.build, watch);
