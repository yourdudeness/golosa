
function calculateTotalValue(length) {
  var minutes = Math.floor(length / 60),
    seconds_int = length - minutes * 60,
    seconds_str = seconds_int.toString(),
    seconds = seconds_str.substr(0, 2),
    time = minutes + ':' + seconds

  return time;
}

function calculateCurrentValue(currentTime) {
  var current_hour = parseInt(currentTime / 3600) % 24,
    current_minute = parseInt(currentTime / 60) % 60,
    current_seconds_long = currentTime % 60,
    current_seconds = current_seconds_long.toFixed(),
    current_time = (current_minute < 10 ? "0" + current_minute : current_minute) + ":" + (current_seconds < 10 ? "0" + current_seconds : current_seconds);

  return current_time;
}

function initProgressBar() {
  var player = document.getElementById('player');
  var length = player.duration
  var current_time = player.currentTime;

  // calculate total length of value
  var totalLength = calculateTotalValue(length)
  jQuery(".end-time").html(totalLength);

  // calculate current value time
  var currentTime = calculateCurrentValue(current_time);
  jQuery(".start-time").html(currentTime);

  var progressbar = document.getElementById('seekObj');
  progressbar.value = (player.currentTime / player.duration);
  progressbar.addEventListener("click", seek);

  if (player.currentTime == player.duration) {
    $('#play-btn').removeClass('pause');
  }

  function seek(evt) {
    var percent = evt.offsetX / this.offsetWidth;
    player.currentTime = percent * player.duration;
    progressbar.value = percent / 100;
  }
};

function initPlayers(num) {
  // pass num in if there are multiple audio players e.g 'player' + i

  for (var i = 0; i < num; i++) {
    (function() {

      // Variables
      // ----------------------------------------------------------
      // audio embed object
      var playerContainer = document.getElementById('player-container'),
        player = document.getElementById('player'),
        isPlaying = false,
        playBtn = document.getElementById('play-btn');

      // Controls Listeners
      // ----------------------------------------------------------
      if (playBtn != null) {
        playBtn.addEventListener('click', function() {
          togglePlay()
        });
      }

      // Controls & Sounds Methods
      // ----------------------------------------------------------
      function togglePlay() {
        if (player.paused === false) {
          player.pause();
          isPlaying = false;
          $('#play-btn').removeClass('pause');

        } else {
          player.play();
          $('#play-btn').addClass('pause');
          isPlaying = true;
        }
      }
      //==========================================================
      const PlayButton = $('.demo-voice__button.button--play');
      const modalAudio =  $('.modal-sample-voice');

      PlayButton.on('click', function () {
        if($(this).hasClass('button--play')) {      //this Play

        //get audio file
        let audioPath = $(this).attr('data-audio-src');
        let authorName = $(this).parents('.speaker-grid__item').find('.text__name').html();
        let voiceName = $(this).next().html();
        $(modalAudio).find('audio source').attr('src', audioPath);

        player.load();
        player.currentTime = 0;
        togglePlay();

        modalAudio.find('.speaker-info__name').html(authorName);
        modalAudio.find('.speaker-info__audio').html(voiceName);

        modalAudio.addClass('modal-sample-voice--active');
        PlayButton.removeClass('button--stop'); //clear all
        PlayButton.addClass('button--play');    //clear all
        $(this).removeClass('button--play');
        $(this).addClass('button--stop');

        } else {                                      //this Stop
          player.pause();

          $(modalAudio).find('audio source').attr('src', '');
          modalAudio.removeClass('modal-sample-voice--active');

          PlayButton.removeClass('button--stop');
          PlayButton.addClass('button--play');
        }
      });

      //about voice

      $('.about-audio__button').on('click', function () {
        let audioPath = $(this).attr('data-audio-src');
        let authorName = 'MediaMaster.pro';
        let voiceName = 'Послушайте о нас';
        $(modalAudio).find('audio source').attr('src', audioPath);

        player.load();
        player.currentTime = 0;
        togglePlay();

        modalAudio.find('.speaker-info__name').html(authorName);
        modalAudio.find('.speaker-info__audio').html(voiceName);
        modalAudio.addClass('modal-sample-voice--active');
      });

      $('.modal-sample-voice__close').on('click',function () {
        player.pause();
        $(modalAudio).find('audio source').attr('src', '');
        PlayButton.removeClass('button--stop');
        PlayButton.addClass('button--play');
        modalAudio.removeClass('modal-sample-voice--active');
      });

    }());
  }
}

initPlayers(jQuery('#player-container').length);

//sample voice modal

