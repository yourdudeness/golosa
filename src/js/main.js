$(document).ready(function () {
  

  function noScroll() {
    $('body').toggleClass('no-scroll');
    offset = window.pageYOffset;
    document.body.style.position = 'fixed';
    document.body.style.top = -offset + 'px';
  };
  function yesScroll() {
    $('body').toggleClass('no-scroll');
    document.body.style.position = '';
    document.body.style.width = '';
    document.body.style.top = '';
    window.scroll(0, offset);
  };

  // Sticky header

  function sticky() {
    if($(window).width() > 810) {
	    $( '.left-block__title' ).hide( );
      $( '.right-block__contacts' ).hide( );
      $('.header').addClass('sticky-header');
      let textNav = $('.header__navigation').html();
      let newNav = $('.header__container-nav');
      newNav.html(textNav);
      $('.header__navigation').hide();
      $('.header__container-nav').show();
      $('.main-screen').css('padding', '15rem 0 0 0');

    }
    else {
      $('.header').addClass('sticky-header');
      $('.main-screen').css('padding', '11rem 0 0 0');
      
    }
  };

  function noSticky() {
    if($(window).width() > 810) {
	    $( '.left-block__title' ).show( );
      $( '.right-block__contacts' ).show( );
      $('.header').removeClass('sticky-header');
      $('.header__container-nav').hide();;
      $('.header__navigation').show();
      $('.main-screen').css('padding', '0 0 0 0');

    }
    else {
      $('.header').removeClass('sticky-header');
      $('.main-screen').css('padding', '1.5rem 0 0 0');
    }
  };

  $(window).scroll(function(){
    if($(document).scrollTop() > 0)
  {
      sticky();
  }
  else
    {
       noSticky();
    }
  });


  //menu
  $('.right-block__menu-caller').on('click', function () {
    $('.navigation__menu').toggleClass('menu--active');
    $('.navigation__bg-navigation').toggleClass('bg-navigation--active');
    noScroll();
  });
  //Close menu on close or click out
  $('.menu__close-button, .navigation__bg-navigation').on('click', function () {
    closeMenu();
  });

  function closeMenu () {
    $('.navigation__menu').removeClass('menu--active');
    $('.navigation__menu').removeClass('menu--active');
    $('.navigation__bg-navigation').removeClass('bg-navigation--active');
    yesScroll();
  };

  //scroll on menu {
  // $('.navigation__menu .anchor').on('click', function () {
  //   closeMenu();
  // });

//Modal windows



  //scroll to anchor
	$('.header').on('click', '.anchor', function(e) {
	  if($(window).width() <= 768) {
	    closeMenu();
    }
		var anchor = $(this);
		$('html, body').stop().animate({
		scrollTop: $(anchor.attr('href')).offset().top -100
		}, 400);
		e.preventDefault();
	});

	//Slider vehicle
  $('.portfolio-block__slider').slick({
    slidesToShow: 1,
    infinite: true,
    dots: false,
    arrows: true,
    adaptiveHeight: false,
    mobileFirst: true,
    variableWidth: false,
    prevArrow: '<button type="button" class="slick-prev button button--play"></button>',
    nextArrow: '<button type="button" class="slick-next button button--play"></button>',
    responsive: [
      {
        breakpoint: 1366,
          settings: {
            variableWidth: true,
          }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2
        }
      }
    ]
  });

  //slider header work
  var headerSlider = $('.main-screen__work-sample');

  headerSlider.slick({
    slidesToShow: 1,
    autoplay: false,
    autoplaySpeed: 3000,
    speed: 300,
    infinite: true,
    dots: false,
    arrows: false,
    vertical: true,
    touchMove: true,
  });


 
  $('.services-list__item').on('click', function() {
    let activeItem = $('.title__select-text');
    $('.item--hide-effect').html(activeItem.html());
    let selectItem = $(this);
    let selectItemWork = $(this).attr('data-duration-work');
    
    $('.title__select-text').html(selectItem.html());
    $('.duration-work').html(selectItemWork);   
    $(this).html($('.item--hide-effect').html());
    headerSlider.slick('slickNext');
    });

 
  //mobile main-screen slider

  var mobileSlider = $('.mobile-info__services-list');

  mobileSlider.slick({
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    speed: 300,
    infinite: true,
    dots: false,
    arrows: false,
    variableWidth: true
  });

  $('.mobile-info__services-list').on('mouseover', function() {
    $(this).on('mousewheel', function (e) {
      e.preventDefault();
      mobileSlider.slick('slickNext');
    });
  });

  mobileSlider.on('beforeChange', function(){
    showServiceMobile();
    mobileSliderVideo.slick('slickNext');

  });

  function showServiceMobile(){  
    let mobileServiceText = $('.mobile-info__services-list .slick-list .slick-track .slick-active');
    let getServiceText = mobileServiceText.attr('data-duration-work');
    $('.mobile-info__data .duration-work').html(getServiceText);

  };

var mobileSliderVideo = $('.main-screen__mobile-work-sample');

  mobileSliderVideo.slick({
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    speed: 300,
    infinite: true,
    dots: false,
    arrows: false,
    variableWidth: true,
    adaptiveHeight: true, // ?
    centerMode: true,
  });



  //price tabs content
  const tabButton = $('.tabs-list__item');
  tabButton.each(function () {
    let tab = '#'+ $(this).attr('data-tab');
    let mobileContent = $(tab).html();
    let targetBlock = $(this).next().find('.tabs-list__content-mobile');

    targetBlock.html(mobileContent);
    if( !$(this).hasClass('item--current') ) {
      $(this).next().slideUp();
    }

  });

  // show/hide mobile tabs on click
  $('.tabs-list__item').on('click', function () {
    let mobileTab = $('.tabs-list__content-mobile');

    if ($(window).width() > 767) {            //desktop

       $('.tabs-list__item').removeClass('item--current mobile-item--current');
       $(this).addClass('item--current mobile-item--current');

       $('.tabs-block__tab').removeClass('tab--current');
       let currentTab = '#'+ $(this).attr('data-tab');
       $(currentTab).addClass('tab--current');

    } else {                                  //mobile

      if( $(this).hasClass('item--current') ) {
        $(this).removeClass('item--current');
        $(this).next().slideUp();
      } else {

        $('.tabs-list__item').removeClass('item--current mobile-item--current');
        $(this).addClass('item--current mobile-item--current');
        $(this).next().slideDown();
        $('html, body').stop().animate({
          scrollTop: $(this).offset().top
          }, 400);
         }
    }
  });

  //disabled button play without audio
  $('.demo-voice__button').each(function () {
    if (!$(this).attr('data-audio-src')) {
      $(this).attr('disabled', 'true');
    }
  });


  //wide video
  $('.video__button-wide').on('click',function () {
    $('.content__video').toggleClass('video--wide');
    $('.video__video-info').toggleClass('video-info--small');
    $(this).toggleClass('button-wide--active');
  });

  $('.content__feedback').on('click', function () {
    let videoBlock = $('.content__video');
    if(videoBlock.hasClass('video--wide')) {
      $('.content__video').removeClass('video--wide');
      $('.video__button-wide').removeClass('button-wide--active');
      $('.video__video-info').removeClass('video-info--small');
    }
  });


});
