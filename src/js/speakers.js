  //speakers
  var countToShow = 3;
  const windowWidth = $(window).width();
  if (windowWidth > 576) {
    countToShow = 6;
  } else if (windowWidth > 992) {
    countToShow = 9;
  }

const speakers = $('.speaker-grid__item');
const countSpeakers = speakers.length;

$('.total-voices').html(countSpeakers);

var countHideSpeakers = (countSpeakers - countToShow > 0) ? countSpeakers - countToShow : 0;
if (!countHideSpeakers) {
  $('.more-demo-voices__button').attr('disabled', true);
  $('.more-demo-voices__text').addClass('text--disabled');
}
$('.available-voice').html(countHideSpeakers);

  //load more demo voices {
speakers.slice(0, countToShow).addClass('item--visible');

$('.more-demo-voices__button').on('click', function (e) {
  e.preventDefault();

  let speakerList = $('.speaker-grid__item:hidden');
  $(speakerList).slice(0, countToShow).slideDown();

  countHideSpeakers = (countHideSpeakers - countToShow > 0) ? countHideSpeakers - countToShow : 0;
  $('.available-voice').html(countHideSpeakers);

  //count hide speakers
  if ($(speakerList).length < countToShow) {
    $('.more-demo-voices__button').attr('disabled', true);
    $('.more-demo-voices__text').addClass('text--disabled');
  }

});
