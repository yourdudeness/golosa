var map = document.querySelector('#map');

  var onIntersection = function(entries) {
    for (const entry of entries) {
      if (entry.intersectionRatio > 0) {
         ymaps.ready(mapsInit);
        observer.unobserve(entry.target);
      }
    }
  };

  var observer = new IntersectionObserver(onIntersection);
  observer.observe(map);

  function mapsInit () {
      myMap = new ymaps.Map('map', {
        center: ($(window).width() <= '768') ? [56.296053, 44.004044] : [56.297046, 44.016874],
        zoom: 15,
        controls: ['zoomControl']
      });
      var Nizhniy = new ymaps.Placemark([56.296053, 44.004044], {
            balloonContent: ''
            }, {
            hasBalloon: false,
            iconLayout: 'default#image',
            iconImageHref: '/img/marker.png',
            iconImageSize: [63, 77],
            iconImageOffset: [-32, -77]
        });
        myMap.geoObjects.add(Nizhniy);
        myMap.behaviors.disable('scrollZoom');
  };






