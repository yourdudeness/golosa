$(document).ready(function () {


    function noScroll() {
    $('body').toggleClass('no-scroll');
    offset = window.pageYOffset;
    document.body.style.position = 'fixed';
    document.body.style.top = -offset + 'px';
  };

  function yesScroll() {
    $('body').toggleClass('no-scroll');
    document.body.style.position = '';
    document.body.style.width = '';
    document.body.style.top = '';
    window.scroll(0, offset);
  };

  function checkAndEnableScroll() {
    if ($('.fancybox-content').length == 0) {
      $('body').removeClass('no-scroll');
    }
  }


//all modal
  $('[data-fancybox]').fancybox({
    toolbar: false,
    smallBtn: true,
    touch: false,
    afterLoad: function () {
      noScroll();
    },
    afterClose: function () {
      $('.order-form').trigger('reset');
      yesScroll();
      checkAndEnableScroll();
    },
  });

  $('.modal__footer-close').on('click', function () {
    $.fancybox.close({
      src: '.modal-privacy',
    });
  });

  //privacy modal
  $('.privacy-label__link').on('click', function () {
    $.fancybox.open({
      src: '#modal-privacy',
      type: 'inline',
      afterLoad: function () {
        $('.fancybox-close-small').focus();
        $('.privacy-text__link').blur();
        noScroll();
      },
      afterClose: function () {
        yesScroll();
        checkAndEnableScroll()
      },
    });
  });

  //Scroll to top
  $('.scroll-top__button').on('click', function () {
    $('.fancybox-slide--html').animate( {scrollTop: '0px'}, 300);
  });


//Modal calc speaker

  $('.price__order-speaker').on('click', function () {
    let author = $(this).parents('.speaker-grid__item').find('.text__name').attr('data-speaker-name');
    let oldTitle = $('.modal-calc .order-content__title').html();
    let newTitle = 'Посчитать проект с озвучкой от <span class="speaker-name">' + author + '</span>';
    $('.modal-calc .order-content__title').html(newTitle);
    $.fancybox.open({
        src: '#modal-calc',
        type: 'inline',
        opts: {
          afterLoad: function () {
            noScroll();
          },
          afterClose: function () {
            yesScroll();
            $('.order-form').trigger('reset');
            $('.modal-calc .order-content__title').html(oldTitle);
            checkAndEnableScroll();
          }
        }
      });
  });

  //Modal video
  //call modal on click from title
  $('.portfolio-item__title').on('click', function () {
    $(this).prev('.portfolio-item__button').click();
  });

  //call modal on click from label
   $('.work-sample__info').on('click', function () {
    if ($(window).width() <= 992) {
       $(this).parents('.work-sample__slide').find('.promo-video__button').click();
     }
  });

  $('.portfolio-item__button, .promo-video__button').on('click', function () {
    let brandLogo = $(this).attr('data-brand-logo');
    let speaker = $(this).attr('data-speaker');
    let videoPath = $(this).attr('data-src');

    $('.modal-video iframe').attr('src', videoPath + '?autoplay=1&autopause=0&loop=0&title=0&byline=0&portrait=0');
    $('.modal-video .video-info__brand .brand__img').attr('src', brandLogo);
    $('.modal-video .video-info__speaker .text-select').html(speaker);
    $.fancybox.open({
        src: '#modal-video',
        type: 'inline',
        opts: {
          afterLoad: function () {
            noScroll();
          },
          afterClose: function () {
            $('.order-form').trigger('reset');
            $('.modal-video iframe').attr('src','');
            $('.modal-video .video-info__speaker .text-select').html('');
            yesScroll();
            checkAndEnableScroll();
          }
        }
      });
  });


});