//Mask input
    var phoneField = $('.input--tel');
    phoneField.mask('+7 999 999 99 99',{placeholder:'_'});

//сheck privacy
  $('.order-form__checkbox:checkbox').change(function () {
    var check = $(this).prop('checked');
    if (check) $(this).parents('form').find('.order-form__button').removeAttr('disabled');
    else $(this).parents('form').find('.order-form__button').attr('disabled', true);
  });


  //Check validate input
  $('.order-form').on('submit', function (e) {
    e.preventDefault();
    $(this).find('.order-form__input').each(function () {
      $(this).removeClass('input--invalid');
      if ($(this).val() == '') {
        $(this).addClass('input--invalid');
      }
    });

    var countEmpty = $(this).find('.input--invalid').length;

    if (countEmpty > 0) { return false
    } else {

      console.log('send message');
      $.fancybox.open({
        src: '#modal-assessment',
        type: 'inline',
        opts: {
          afterClose: function () {
            location.reload();
          }
        }
      });
      //Ajax отправка

    }

  });
