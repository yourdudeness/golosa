$(document).ready(function() {

	//Загрузка изображения на сервер
	$('.upload-field').upload({
  		action: "#",
  		autoUpload: false,
			label: 'Прикрепить файлы',
			chunked: true,
	})
	.on('queued.upload', onQueued);

	function onQueued(e, files) {
		let html = '';

    for (let i = 0; i < files.length; i++) {
			html = `
			<li class="upload-files-item" data-index="${files[i].index}">${files[i].name}</li>
			`
		}

    $(this).parents('form').find('.upload-files-list').append(html);
  }
});
